﻿using System.Text;
using RabbitMQ.Client;

namespace Sender
{
    /// <summary>
    /// 简单队列
    /// </summary>
    public static class SimpleQueue
    {
        public static void Run()
        {
            var factory = new ConnectionFactory()       // 创建连接工厂对象
            {
                HostName = "127.0.0.1",
                Port = 5672,
                UserName = "root",
                Password = "123456"
            };
            var connection = factory.CreateConnection();    // 创建连接对象
            var channel = connection.CreateModel();         // 创建连接会话对象

            string queueName = "queue1";

            // 声明一个队列
            channel.QueueDeclare(
                queue: queueName,   // 队列名称
                durable: true,     // 是否持久化，true持久化，队列会保存磁盘，服务器重启时可以保证不丢失相关信息
                exclusive: false,   // 是否排他，如果一个队列声明为排他队列，该队列仅对时候次声明它的连接可见，并在连接断开时自动删除
                autoDelete: false,  // 是否自动删除，自动删除的前提是：至少有一个消费者连接到这个队列，之后所有与这个队列连接的消费者都断开时，才会自动删除
                arguments: null     // 设置队列的其他参数
            );

            string str = string.Empty;

            do
            {
                Console.WriteLine("发送内容：");
                str = Console.ReadLine()!;

                // 消息内容
                byte[] body = Encoding.UTF8.GetBytes(str);

                IBasicProperties msg_pro = channel.CreateBasicProperties();
                msg_pro.DeliveryMode = 2;

                // 发送消息
                channel.BasicPublish("", queueName, msg_pro, body);

                // Console.WriteLine("成功发送消息：" + str);
            } while (str.Trim().ToLower() != "exit");

            channel.Close();
            connection.Close();


            //笔记
            //如果消息想要从Rabbit崩溃中恢复，那么消息必须满足以下条件：
            //1.把它的投递默认选项设置为持久化
            //2.发送到持久化的交换机
            //3.到达持久化的队列
        }
    }
}
